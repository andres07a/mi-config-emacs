(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (yasnippet-snippets zenburn-theme windresize use-package undo-tree telephone-line rainbow-mode paredit nlinum-relative magit hydra hungry-delete highlight-numbers highlight-escape-sequences godoctor go-snippets go-guru go-eldoc gitignore-mode gitconfig-mode flyspell-correct-ivy flycheck f counsel-projectile company-statistics company-quickhelp company-go better-defaults all-the-icons ag ace-window))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 3.0)))))
