;; package.el, instrucción obligatoria que debe ir de primero en la
;; configuración, siempre.
(package-initialize nil)
;; desactivación de pantalla de inicio/bienvenida
(setq inhibit-startup-screen t)
;; Nos ponemos serios con la seguridad de la conexión en Emacs, así no nos
;; ponen malware cuando instalamos paquetes desde Melpa con ataques "Man in the
;; middle"
(setq network-security-level 'low)
;; repositorios de paquetes
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("melpa" . "http://melpa.org/packages/")))

;; actualizamos la base de datos de paquetes de `melpa' y `gnu'
(unless package-archive-contents
  (package-refresh-contents))
;; instalamos `use-package' si no esta disponible
(when (not (package-installed-p 'use-package))
  (package-install 'use-package))
;; activamos y configuramos `use-package'
(require 'use-package)
;; `use-package' siempre se asegura que el paquete esta instalado
(setf use-package-always-ensure t)
(use-package zenburn-theme
  ;; cambiamos el tema de Emacs a zenburn
  :config
  (load-theme 'zenburn t))
(use-package existefuente
  :ensure nil
  :preface (provide 'existefuente)
  :init
  (defun font-exists-p (font)
    "Comprueba si una tipografía existe. Sacado de https://redd.it/1xe7vr"
    (if (not (find-font (font-spec :name font)))
        nil
      t))
  (defun mi/arregla-emojis (&optional frame)
    ;; es importante que Symbola este instalada en el sistema
    ;;  http://users.teilar.gr/~g1951d/Symbola.zip
    ;; Para NS/Cocoa
    (when (eq system-type 'darwin)
      (set-fontset-font t 'symbol (font-spec :family "Apple Color Emoji") frame 'prepend))
    ;; Para GNU/Linux y Windows
    (when (and (font-exists-p "Symbola") (or (eq system-type 'gnu/linux) (eq system-type 'windows-nt)))
      (set-fontset-font t 'symbol (font-spec :size 20 :name "Symbola") frame 'prepend))))
(use-package misfuentes
  :ensure nil
  :preface (provide 'misfuentes)
  :init
  (defun mi/font-set (&optional frame)
    (when (framep frame)
      (select-frame frame))
    (cond
     ((font-exists-p "Fira Code") (set-frame-font "Fira Code 10") (add-to-list 'default-frame-alist '(font . "Fira Code-10")))
     ((font-exists-p "Monoisome") (set-frame-font "Monoisome 9") (add-to-list 'default-frame-alist '(font . "Monoisome-9")))
     ((font-exists-p "Monoid") (set-frame-font "Monoid 9") (add-to-list 'default-frame-alist '(font . "Monoid-9")))
     ((font-exists-p "Source Code Pro") (set-frame-font "Source Code Pro 10") (add-to-list 'default-frame-alist '(font . "Source Code Pro-10")))))
  :config
  (add-hook 'after-make-frame-functions #'mi/arregla-emojis)
  (add-hook 'after-make-frame-functions #'mi/font-set)
  (unless (daemonp)
    (mi/arregla-emojis)
    (mi/font-set)))
(defun smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.
Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.
If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))
  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))
  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))
;; remap C-a to `smarter-move-beginning-of-line'
(global-set-key [remap move-beginning-of-line]
                'smarter-move-beginning-of-line)
(use-package nlinum
  ;; Enumera lineas de código
  :disabled t 
  :init
  (defun ancho-nlinum-mode-hook ()
    "Calcula el ancho de los números para evitar feos saltos al desplazarse"
    (when nlinum-mode
      (setq-local nlinum-format (concat "%" (number-to-string
                                             (ceiling (log (max 1 (/ (buffer-size) 80)) 10)))
                                        "d"))))
  (defun initialize-nlinum (&optional frame)
    "Flanque de error en nlinum al usar Emacs como demonio"
    (require 'nlinum)
    (add-hook 'prog-mode-hook 'nlinum-mode))
  (add-hook 'nlinum-mode-hook #'ancho-nlinum-mode-hook)
  (if (daemonp)
      (progn
        (add-hook 'window-setup-hook #'initialize-nlinum)
        (defadvice make-frame (around toggle-nlinum-mode compile activate)
          (nlinum-mode -1) ad-do-it (nlinum-mode 1)))
    (add-hook 'prog-mode-hook #'nlinum-mode)))
(use-package telephone-line
  :after magit-mode all-the-icons
  :preface (defun mi/vc-state ()
             (if vc-mode
                 (vc-state (buffer-file-name (current-buffer)))
               nil))
  :init
  (setf telephone-line-height 30)
  (custom-set-faces
   '(mode-line ((t (:box nil))))
   '(mode-line-inactive ((t (:box nil))))
   '(mode-line-highlight ((t (:box nil)))))
  :config
  (telephone-line-defsegment* mi-buffer-vc-modified-segment
    (list (cond ((eq (mi/vc-state) 'edited)
                 (propertize (format " %s" (all-the-icons-faicon "pencil")) 'face `(:height 1.3 :family ,(all-the-icons-faicon-family))
                             'display '(raise -0.1) 'help-echo "Buffer modificado, cambios sin registrar."))
                ((buffer-modified-p)
                 (propertize (format " %s" (all-the-icons-faicon "pencil")) 'face `(:foreground "tomato" :height 1.3 :family ,(all-the-icons-faicon-family))
                             'display '(raise -0.1) 'help-echo "Buffer modificado.")))
          (cond ((eq (mi/vc-state) 'missing)
                 (propertize (format " %s " (all-the-icons-faicon "trash")) 'face `(:height 1.3 :family ,(all-the-icons-faicon-family))
                             'display '(raise -0.1) 'help-echo "Archivo sólo existe en VCS, no en el disco duro."))
                ((eq (mi/vc-state) 'ignored)
                 (propertize (format " %s " (all-the-icons-faicon "ban")) 'face `(:height 1.3 :family ,(all-the-icons-faicon-family))
                             'display '(raise -0.1) 'help-echo "Archivo ignorado"))
                ((eq (mi/vc-state) 'added)
                 (propertize (format " %s " (all-the-icons-faicon "plus")) 'face `(:height 1.3 :family ,(all-the-icons-faicon-family))
                             'display '(raise -0.1) 'help-echo "Archivo será registrado en VCS en el siguiente commit."))
                ((eq (mi/vc-state) 'unregistered)
                 (propertize (format " %s " (all-the-icons-faicon "question")) 'face `(:height 1.3 :family ,(all-the-icons-faicon-family))
                             'display '(raise -0.1) 'help-echo "Archivo sin registrar al VCS.")))))
  (telephone-line-defsegment mi-line-buffer-segment
    (telephone-line-raw mode-line-buffer-identification t))
  (telephone-line-defsegment* mi-vc-info
    (when vc-mode
      (cond ((string-match "Git[:-]" vc-mode)
             (let ((branch (mapconcat 'concat (cdr (split-string vc-mode "[:-]")) "-")))
               (concat
                (propertize (format " %s" (all-the-icons-alltheicon "git")) 'face `(:foreground "orange" :height 1.3) 'display '(raise -0.1))
                " · "
                (propertize (format "%s" (all-the-icons-octicon "git-branch"))
                            'face `(:foreground "yellow" :height 1.3 :family ,(all-the-icons-octicon-family))
                            'display '(raise -0.1))
                (propertize (format " %s" branch) 'face `(:foreground "yellow" :height 0.9)))))
            ((string-match "SVN-" vc-mode)
             (let ((revision (cadr (split-string vc-mode "-"))))
               (concat
                (propertize (format " %s" (all-the-icons-faicon "cloud")) 'face `(:height 1.3) 'display '(raise -0.1))
                (propertize (format " · %s" revision) 'face `(:height 0.9)))))
            (t (format "%s" vc-mode)))))
  (telephone-line-defsegment* mi-flycheck-status
    (let* ((text (pcase flycheck-last-status-change
                   (`finished (if flycheck-current-errors
                                  (let ((count (let-alist (flycheck-count-errors flycheck-current-errors)
                                                 (+ (or .warning 0) (or .error 0)))))
                                    (propertize (format " ✖ %s problema%s" count (if (> count 1) "s" "")) 'face `(:foreground "orange")))
                                (propertize " ✔ Sin problemas" 'face `(:foreground "dark grey"))))
                   (`running     (propertize " ⟲ En ejecución" 'face `(:foreground "deep sky blue")))
                   (`no-checker  (propertize " ⚠ No existe revisor" 'face `(:foreground "dim grey")))
                   (`not-checked (propertize " ✖ Sin revisar" 'face `(:foreground "dim grey")))
                   (`errored     (propertize " ⚠ Error" 'face `(:foreground "tomato")))
                   (`interrupted (propertize " ⛔ Interrumpido" 'face `(:foreground "tomato")))
                   (`suspicious  ""))))
      (propertize text
                  'help-echo "Muestra errores detectados por Flycheck"
                  'local-map (make-mode-line-mouse-map
                              'mouse-1 (lambda () (interactive) (flycheck-list-errors))))))
  (setf telephone-line-lhs
        '((accent . (mi-line-buffer-segment mi-buffer-vc-modified-segment))
          (nil .  (mi-vc-info mi-flycheck-status))))
  (setf telephone-line-rhs '((nil . (telephone-line-misc-info-segment telephone-line-major-mode-segment))
                             (accent . (telephone-line-minor-mode-segment telephone-line-position-segment))))
  (telephone-line-mode 1))
(use-package better-defaults
  ;; Mejores ajustes por defecto para Emacs
  ;; Configuraciones automaticas se guardan en el archivo `custom.el'
  :init
  ;; todo aqui es evaluado **antes** de que el paquete `algun-paquete' es
  ;; evaluado
  (setf custom-file (expand-file-name "custom.el" user-emacs-directory))
  (defalias 'yes-or-no-p 'y-or-n-p)
  (global-set-key (kbd "C-x k") 'kill-this-buffer)
  :config
  ;; todo aqui es evaluado **despues** de que el paquete `algun-paquete' es
  ;; evaluado
  (ido-mode nil) ;; desactivamos ido-mode porque usaremos ivy-mode
  (setf delete-active-region t) ;; borra con DEL o SUPR la región marcada
  ;; carga las configuraciones automaticas
  (load custom-file))
(use-package prog-mode
  ;; Todos los modos mayores que heredan de prog-mode son afectados
  ;; por esta configuración
  :after rainbow-mode hes-mode highlight-numbers-mode
  :ensure nil
  :preface
  (provide 'prog-mode)
  :init
  (defun mi/prog-mode ()
    (set (make-local-variable 'fill-column) 79)
    (set (make-local-variable 'comment-auto-fill-only-comments) t)
    ;; Nota: M-q rellena las columnas del párrafo actual
    ;;       M-o M-s centra una linea de texto
    (auto-fill-mode t)
    (highlight-numbers-mode)
    (hes-mode)
    (electric-pair-mode)
    (rainbow-turn-on)
    (flyspell-prog-mode))
  (add-hook 'prog-mode-hook #'mi/prog-mode)
  :config
  (bind-key "RET" 'newline-and-indent)
  (bind-key* "C-M-," 'comment-dwim))
(use-package text-mode
  ;; Todos los modos mayores que heredan de text-mode son afectados por esta
  ;; configuración
  :ensure nil
  :preface
  (provide 'text-mode)
  (defun mi/text-mode ()
    (flyspell-mode)
    (set (make-local-variable 'fill-column) 100)
    (turn-on-visual-line-mode))
  :config
  (add-hook 'text-mode-hook #'mi/text-mode))
(use-package auto-fill
  ;; Rompe lineas de texto de manera automática en cierta columna
  :ensure nil
  :diminish auto-fill-function
  :preface (provide 'auto-fill))
(use-package flyspell
  ;; correccion ortografica
  ;; NOTA: Preparación previa para Windows
  ;;   https://www.emacswiki.org/emacs/AspellWindows
  :diminish flyspell-mode
  :ensure nil)
(use-package flyspell-correct-ivy
  ;; Una mejor interfaz para flyspell, usando ivy
  :after flyspell-mode
  :bind (:map flyspell-mode-map ("C-." . flyspell-correct-previous-word-generic))
  :init
  (setf flyspell-correct-auto-delay 2.0))
(use-package shackra/ispell-dict-switch
  :bind (("<f8>" . shackra/ispell-cycle-dict))
  :ensure nil
  :preface (provide 'shackra/ispell-dict-switch)
  :config
  (setf shackra/ispell-dict-list (list "es" "en"))
  :init
  (defvar shackra/ispell-dict-list (list) "Lista de diccionarios para cambiar")
  (defun shackra--ispell-current-dict ()
    "Obtiene el diccionario actual o retorna el establecido por defecto"
    (if ispell-current-dictionary
        (cl-position ispell-current-dictionary shackra/ispell-dict-list :test 'string=)
      (if (stringp (getenv "LANG"))
          (cl-position (nth 0 (split-string (getenv "LANG") "_")) shackra/ispell-dict-list :test 'string=)
        0))) ;; retorna español mientras tanto
  (defun shackra/ispell-cycle-dict ()
    "Cambia de un diccionario a otro"
    (interactive)
    (let ((dict-list-size (- (length shackra/ispell-dict-list) 1))
          (dict-current-index (shackra--ispell-current-dict)))
      (if (> (+ dict-current-index 1) dict-list-size)
          (setf dict-current-index 0)
        (setf dict-current-index (+ dict-current-index 1)))
      ;; luego de realizar la matemática, cambiamos el diccionario
      (ispell-change-dictionary (nth dict-current-index shackra/ispell-dict-list)))
    ;; Nuevo diccionario, nada de palabras subrayadas
    (flyspell-delete-all-overlays)
    ;; Mandamos a revisar la ortografía del párrafo en que estamos
    (flyspell-region (line-beginning-position) (line-end-position))))
(use-package auto-fill
  :ensure nil
  :diminish auto-fill-function
  :preface (provide 'auto-fill))
(use-package rainbow-mode
  ;; Colorea numeros hexadecimales según su valor RGB y nombres de colores en
  ;; inglés
  )
(use-package highlight-escape-sequences
  ;; Resalta secuencia de escape de texto como \n
  :config
  (put 'hes-escape-backslash-face 'face-alias 'font-lock-builtin-face)
  (put 'hes-escape-sequence-face 'face-alias 'font-lock-builtin-face))
(use-package highlight-numbers
  ;; Resalta numeros
  )
(use-package paredit
  ;; Un modo menor para la edición de paréntesis. Para aprender qué hace este
  ;; modo menor y sus posibilidades, ver
  ;; «The Animated Guide to Paredit»
  ;;   http://danmidwood.com/content/2014/11/21/animated-paredit.html
  :diminish paredit-mode ;; elimina el `lighter' del mode-line
  :config
  (add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
  (add-hook 'lisp-mode-hook 'enable-paredit-mode)
  (add-hook 'lisp-interaction-mode-hook 'enable-paredit-mode))
(use-package hungry-delete
  ;; borra grandes bloques de espacios en blanco rápidamente, activado para
  ;; todos los modos mayores.
  :diminish hungry-delete-mode
  :config
  (global-hungry-delete-mode))
(use-package recentf
  ;; configura `recentf'
  :ensure nil
  :init
  (setf recentf-max-saved-items 100)
  :config
  (add-to-list 'recentf-exclude ".git/")
  (add-to-list 'recentf-exclude ".hg/")
  (add-to-list 'recentf-exclude "elpa/")
  (add-to-list 'recentf-exclude "\\.emacs.d/org-clock-save.el\\'")
  (add-to-list 'recentf-exclude "INBOX/"))
(use-package swiper
  ;; "Zorro no te lo lleves!"  Swiper busca texto más rapido que
  ;; `isearch'. También provee ivy que es una interfaz similar a Helm,
  ;; pero más rapida y sencilla.
  :demand t ;; Demanda la evaluación del paquete. Cuando se usa
  ;; `:bind' la evaluación del paquete es atrasada hasta que
  ;; el usuario use los atajos del teclado, ver más en https://github.com/jwiegley/use-package#key-binding
  
  ;; reemplazamos los atajos de `isearch' con comandos de `swiper'
  :bind (:map global-map
              ("C-s" . swiper)
              ("C-r" . swiper)
              ("C-c C-r" . ivy-resume))
  :init
  (setf ivy-count-format "(%d/%d) ") ;; Cómo muestra el conteo ivy
  (setf ivy-height 15) ;; altura de ivy, equivale a la cantidad de candidatos a mostrar
  :config
  ;; ivy debe ignorar estos buffers, especificado con expresiones regulares de Emacs
  (setf ivy-ignore-buffers (append ivy-ignore-buffers '("\\*weechat-relay" "\\*Messages\\*" "\\*elfeed-log\\*" "\\*Help\\*" "\\*Compile-Log\\*")))
  (use-package counsel
    ;; Counsel es un paquete que permite a ivy mostrar variedad de candidatos.
    :demand t
    :config (use-package counsel-projectile) ;; candidatos de `projectile-mode'
    ;; reemplazamos comandos de Emacs con los ofrecidos por Counsel.
    :bind (:map global-map            ;; mapeo global
                ("M-x" . counsel-M-x) ;; comandos interactivos de Emacs
                ("M-y" . counsel-yank-pop) ;; pegar texto
                :map help-map ;; mapeo de ayuda (comienza con `C-h')
                ("f" . counsel-describe-function) ;; describir funciones de Emacs
                ("v" . counsel-describe-variable) ;; describir variables de Emacs
                ("b" . counsel-descbinds) ;; describir atajos de teclado para el modo mayor actual
                :map ctl-x-map ;; mapeo `C-x' (comienza con `C-x', duh)
                ("8 RET" . counsel-unicode-char) ;; caracteres unicode!
                ("l" . counsel-locate) ;; localizar archivos con `locate' de GNU/Linux
                ("f" . counsel-recentf) ;; archivos recientes visitados
                ("C-f" . counsel-find-file))) ;; visitar archivo
  ;; activamos ivy
  (ivy-mode 1))
(use-package company
  ;; un mejor motor de autocompletado que autocomplete-mode
  :diminish company-mode
  :init
  ;; "backends" activados para company.
  (setf company-backends '((company-files
                            company-keywords
                            company-capf
                            company-yasnippet)
                           (company-abbrev company-dabbrev)))
  (setf company-idle-delay 0.3)
  (setf company-tooltip-limit 30)
  (setf company-minimum-prefix-length 1)
  (setf company-echo-delay 0)
  (setf company-auto-complete nil)
  ;; activar company-mode globalmente luego que Emacs este inicializado
  (add-hook 'after-init-hook #'global-company-mode)
  :config
  (use-package company-quickhelp
    ;; Muestra ayuda sobre comandos y variables, sólo funcionará si el
    ;; modo mayor provee soporte para mostrar documentación. Un
    ;; ejemplo: https://github.com/expez/company-quickhelp/issues/45
    :config (company-quickhelp-mode 1))
  (use-package company-statistics
    ;; estadisticas de candidatos escogidos para company-mode, ayuda a
    ;; mejorar el orden de los candidatos.
    :init
    (setf company-statistics-file "~/.company-statistics-cache.el")
    (add-hook 'after-init-hook 'company-statistics-mode)))
(use-package projectile
  ;; manejo de proyectos
  :after ivy-mode
  :diminish projectile-mode
  :init
  (setf projectile-completion-system 'ivy ;; usa ivy para el sistema de completado
        ;; abre archivo de proyecto luego de entrar a un proyecto, usando counsel
        projectile-switch-project-action 'counsel-projectile-find-file)
  (setf
   projectile-file-exists-remote-cache-expire (* 10 60)
   projectile-globally-ignored-files '("TAGS" "\#*\#" "*~" "*.la"
                                       "*.o" "*.pyc" "*.elc" "*.exe"
                                       "*.zip" "*.tar.*" "*.rar" "*.7z"))
  :config
  (add-hook 'after-init-hook #'projectile-global-mode)) ;; arranca projectile en modo global
(use-package ag
  ;; Busquedas más rapidas con el comando de GNU/Linux `ag'
  :if (executable-find "ag"))
(use-package flycheck
  :diminish flycheck-mode
  :init
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)) ;; deshabilita el majadero revisor de documentación
  (setf flycheck-indication-mode 'left-fringe)
  (setf flycheck-display-errors-delay 5.0) ;; tarda 5 segundos en
                                           ;; describir el error en el
                                           ;; minibuffer, para evitar
                                           ;; choques con `eldoc'
  (add-hook 'prog-mode-hook #'flycheck-mode))
(use-package hydra
  ;; Hail Hydra!
  ;;   https://github.com/abo-abo/hydra#description-for-pragmatics
  :preface
  (use-package windmove
    :ensure nil)
  (use-package winner
    :ensure nil
    :config
    (winner-mode 1))
  (use-package windresize)
  (use-package ace-window
    :bind ("M-o" . ace-window) ;; cambia de ventana con M-o
    :init
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0)))))
    (defun --count-frames ()
      "Retorna el numero de frames visibles"
      (let* ((frames (if (daemonp) (butlast (visible-frame-list) 1) (visible-frame-list))))
        (length frames)))
    :config
    (setf aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)))
  :config
  (defhydra hydra-zoom (global-map "<f2>")
    "Acercamiento"
    ("f" text-scale-increase "in")
    ("j" text-scale-decrease "out"))
  (defhydra hydra-avy (:color blue :columns 2)
    "avy jump"
    ("z" avy-goto-line "Ir a la linea...")
    ("x" avy-goto-word-1 "Ir a la palabra...")
    ("c" avy-goto-char-in-line "Ir a la letra en la linea actual...")
    ("v" avy-goto-char "Ir a la palabra (2)..."))
  (bind-key "C-z" 'hydra-avy/body)
  ;; Hydra nos permite hacer magia con la administración de ventanas dentro de
  ;; un marco de Emacs. Varios paquetes estan especificados en el `:preface'
  ;; del macro para hydra
  (defhydra hydra-win (:columns 4 :color amaranth)
    "Manejo de ventanas"
    ("<up>" windmove-up "Cursor ↑")
    ("<left>" windmove-left "Cursor ←")
    ("<down>" windmove-down "Cursor ↓")
    ("<right>" windmove-right "Cursor →")
    ("C-<up>" hydra-move-splitter-up "Astilla ↑")
    ("C-<left>" hydra-move-splitter-left "Astilla ←")
    ("C-<down>" hydra-move-splitter-down "Astilla ↓")
    ("C-<right>" hydra-move-splitter-right "Astilla →")
    ("b" mi/switch-buffer "Cambiar buffer")
    ("f" mi/find-file "Visitar/Crear archivo")
    ("z" (lambda () (interactive) (ace-window 1) (add-hook 'ace-window-end-once-hook 'hydra-win/body)) "Mover cursor a otra ventana")
    ("2" (lambda () (interactive) (split-window-right) (windmove-right)) "Dividir |")
    ("3" (lambda () (interactive) (split-window-below) (windmove-down)) "Dividir -")
    ("c" (lambda () (interactive) (ace-window 4) (add-hook 'ace-window-end-once-hook 'hydra-win/body)) "Intercambiar buffer de ventana")
    ("s" save-buffer "Guardar buffer")
    ("x" delete-window "Borrar ventana")
    ("X" (lambda () (interactive) (ace-window 16) (add-hook 'ace-window-end-once-hook 'hydra-win/body)) "Borrar esta/otra ventana")
    ("1" ace-maximize-window "Maximizar esta ventana")
    ("," (progn (winner-undo) (setf this-command 'winner-undo)) "Deshacer ultimo cambio")
    ("." winner-redo "Rehacer ultimo cambio")
    ("SPC" nil "Salir"))
  (bind-key "M-1" #'hydra-win/body))

(use-package go-mode
  ;; modo mayor para editar archivos .go
  :if (executable-find "go") ;; go tiene que estar instalado en el
                             ;; sistema y disponible desde la terminal
                             ;; de comandos para instalar este paquete.
  :bind (:map go-mode-map
              ("M-." . godef-jump) ;; salta a una definicion
              ("C-c C-a" . go-import-add) ;; añade modulo para importar
              ("C-c C-r" . go-remove-unused-imports) ;; quita importaciones sin usar
              ("C-c C-i" . go-goto-imports) ;; va a la sentencia de importaciones
              ("C-c C-c" . compile)) ;; compila el archivo
  :init
  (use-package f) ;; requerido para `mi/go-update-compile-definition'
  (setf gofmt-show-errors nil) ;; Flycheck se encarga de mostrar errores, gracias.
  (setf gofmt-command "goimports") ;; instalar goimports en el sistema
  (defun mi/go-update-compile-definition ()
    "Actualiza la definición del comando COMPILE para go-mode"
    (interactive)
    (when (eq major-mode 'go-mode)
      (if (string-suffix-p "_test.go" (buffer-name))
          (set (make-local-variable 'compile-command) "go test -bench=. -v && go vet")
        (set (make-local-variable 'compile-command) (format "go build && go vet && ./%s" (f-base (f-dirname (f-full (buffer-name)))))))))
  :config
  (add-hook 'go-mode-hook (lambda () (add-to-list (make-local-variable 'grep-find-ignored-directories) "vendor")))
  (add-hook 'before-save-hook 'gofmt-before-save)
  (add-hook 'go-mode-hook 'mi/go-update-compile-definition)
  (add-hook 'after-save-hook 'mi/go-update-compile-definition))
(use-package company-go
  ;; auto completado para Go usando company
  :if (executable-find "gocode") ;; gocode tiene que estar instalado
                                 ;; en el sistema y disponible desde
                                 ;; la terminal de comandos para
                                 ;; instalar este paquete
  :after go-mode
  :config
  (add-hook 'go-mode-hook (lambda () (add-to-list (make-local-variable 'company-backends) '(company-go :with company-yasnippet)))))
(use-package go-eldoc
  ;; muestra la firma de funciones go en el `minibuffer'
  :after go-mode
  :config
  (add-hook 'go-mode-hook 'go-eldoc-setup))
(use-package go-snippets
  ;; plantillas de codigo de Yasnippet para Go
  :after go-mode)
(use-package godoctor
  ;; refactorización de codigo Go
  :if (executable-find "godoctor") ;; godoctor tiene que estar
                                   ;; instalado en el sistema y
                                   ;; disponible desde la terminal de
                                   ;; comandos para instalar este
                                   ;; paquete
  :after go-mode
  :bind (:map go-mode-map ("C-M-m" . hydra-godoctor/body))
  :config
  (defhydra hydra-godoctor (:color blue :columns 2)
    "Motor de refactorización para Go"
    ("a" godoctor-rename "Renombra identificador, punto actual")
    ("s" godoctor-extract "Refactorizacion en una función, marca actual")
    ("d" godoctor-toggle "Cambia declaración de variable")
    ("f" godoctor-godoc "Crea esqueleto de documentación")))
(use-package go-guru
  ;; Nos dice cosas sobre nuestro codigo para que nosotros no debamos adivinar
  :if (executable-find "guru") ;; guru tiene que estar instalado en
                               ;; el sistema y disponible desde la
                               ;; terminal de comandos para instalar
                               ;; este paquete
  :after go-mode
  :demand t
  :init
  (defun mi/go-mode-set-scope ()
    (when (eq major-mode 'go-mode)
      (set (make-local-variable 'go-guru-scope)
           (concat (projectile-project-root) "..."))))
  :bind (:map go-mode-map
              ("M-." . go-guru-definition)
              ("C-M-g" . hydra-go-guru/body))
  :config
  (defhydra hydra-go-guru (:color blue :columns 2)
    "Contesta preguntas sobre código fuente escrito en Go"
    ("a" go-guru-callees "Receptores de llamada, función bajo punto actual")
    ("s" go-guru-callers "Llamadores, función bajo punto actual")
    ("d" go-guru-callstack "Muestra grafo de llamadas desde una raíz, función bajo punto actual")
    ("f" go-guru-describe "Describe la sintaxis seleccionada, su tipo y métodos")
    ("g" go-guru-freevars "Enumera las variables libres, marca actual")
    ("h" go-guru-referrers "Enumera referencias al objeto, identificador marcado")
    ("j" go-guru-peers "Enumera un set de remitentes/destinatarios para las operaciones enviar/recibir de este canal")
    ("k" go-guru-pointsto "Muestra lo que apunta, expresión marcada")
    ("l" go-guru-implements "Describe la relación de implementación para tipos en un paquete conteniendo el cursor")
    ("ñ" go-guru-whicherrs "Muestra globales, constantes y tipos, expresión marcada (de tipo 'error')"))
  (add-hook 'go-mode-hook #'go-guru-hl-identifier-mode)
  (add-hook 'projectile-mode-hook #'mi/go-mode-set-scope))
(use-package magit
  ;; Una interfaz para Git dentro de Emacs
  ;; Se puede invocar con Projectile usando `C-p v'
  :defer t
  :init
  (setf magit-last-seen-setup-instructions "1.4.0"
        magit-auto-revert-mode nil
        magit-completing-read-function 'ivy-completing-read))
(use-package gitignore-mode
  ;; modo mayor para editar archivos .gitignore
  )
(use-package gitconfig-mode
  ;; modo mayor para editar archivos de configuración git
  )
(use-package all-the-icons
  ;; iconos bonitos dentro de Emacs. NOTA: instalar las tipografias
  ;; que vienen con este paquete en el sistema para Emacs pueda
  ;; mostrar correctamente los iconos
  )
(use-package yasnippet
  :diminish yas-minor-mode
  :init
  ;; http://emacs.stackexchange.com/questions/10431/get-company-to-show-suggestions-for-yasnippet-names
  ;; Add yasnippet support for all company backends
  ;; https://github.com/syl20bnr/spacemacs/pull/179
  (defun company-mode/backend-with-yas (backend)
    (if (or (and (listp backend) (member 'company-yasnippet backend)))
        backend
      (append (if (consp backend) backend (list backend))
              '(:with company-yasnippet))))
  
  (add-hook 'after-init-hook (lambda ()
                               (setf company-backends (mapcar #'company-mode/backend-with-yas company-backends))))
  :config
  (add-hook 'prog-mode-hook #'yas-minor-mode-on))
(use-package eldoc
  :ensure nil
  :diminish eldoc-mode
  :init
  (setf eldoc-idle-delay 1.0))
(use-package undo-tree
  ;; Reemplaza el mecanismo de deshacer/hacer de Emacs con un sistema que trata
  ;; los cambios realizados como un árbol con ramificaciones de cambios.
  ;; para revertir un cambio use `C-x u'. Más información en:
  ;;   http://melpa.milkbox.net/#/undo-tree
  :diminish undo-tree-mode
  :init
  (defadvice undo-tree-make-history-save-file-name
      (after undo-tree activate)
    (setq ad-return-value (concat ad-return-value ".7z")))
  (defadvice undo-tree-visualize (around undo-tree-split-side-by-side activate)
  "Divide la ventana de lado a lado al visualizar undo-tree-visualize"
  (let ((split-height-threshold nil)
        (split-width-threshold 0))
  ad-do-it))
  (setf undo-tree-visualizer-timestamps t)
  (setf undo-tree-visualizer-diff t)
  (setf undo-tree-auto-save-history t)
  :config
  (defalias 'redo 'undo-tree-redo)
  (global-undo-tree-mode 1))
